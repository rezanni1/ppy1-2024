
def process_data(df):
    # Perform some data processing operations
    df['result'] = df['value'] * 2
    return df
